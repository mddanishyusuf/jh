<?php
include 'header.php';
?>
        
        <div class="jh-home-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="jh-content-title" style="color:#800000;"><h2>Conferences</h2></div>
				 <ul class="jh-conf-list">
					<li><a href="register.html">Click here to Register</a> </li>
					 <li><a href="#pay" onclick="check();return false;">Click here to Payment</a> </li>
					 <li><a href="#conf" onclick="check1();return false;">Check All the Conferences</a> </li>
					 </ul>
                    <!--<ul class="jh-conf-list">
					<li>National Conference "JCON 2011" 21 & 22 January 2012 </li>
					<li>National Conference "JCON 2009" 21 & 22 February 2009</li>
					<li>2017-18 Technology and the Corporation Conferences</li>
					
                    </ul> -->
                </div>
				<script>
						function check(){
							document.getElementById("pay").style.display="block";
							document.getElementById("conf").style.display="none";
							
						}
						function check1(){
							document.getElementById("pay").style.display="none";
							document.getElementById("conf").style.display="block";
							
						}
						
				</script>
				
<div class="col-md-9" id="pay" style="display:none;">
					 <center>  <div class="jh-content-title" style="color:#800000;"><h2>Payment</h2></div>
				 
				 <form id="send" method="POST" action="reg1.php">
					 <p>

					<input name="name" placeholder="Enter your Full Name" required="" type="text" required>
					 </p>

					
					<p>
					   
					<input placeholder="Email address *" name="email" required="" type="email"> 
					</p>      

					  <center><input value="&nbsp;&nbsp;&nbsp;Move to Payment &nbsp;&nbsp;" type="submit"></center>
				</form>
					</div>
					
<div class="col-md-9" id="conf" >
                    <h3 class="jh-content-title"><span>All Conferences</span></h3>
                    <div class="row conf-posts" style="color: navy;text-align:justify;font-size: 15px;">
                        <div class="col-md-4">
                            <img src="assets/img/event.png" style="width: 100%">
                        </div>
                        <div class="col-md-8">
                            <h4>National Conference "JCON 2011" 21 & 22 January 2011</h3>
							<p>National Conference "JCON 2011" 21 & 22 January 2011 on the topic - Live workshop on Laparoscopic Surgery "Adoption of Advanced Surgical Technology in Indigenous System of Medicine - A Need of Today" - Chief Guest - Dr. Anil Arora, The Director, Sir Ganga Ram Hospital, New Delhi
	</p>
                </div>
            </div>
            <div class="row conf-posts" style="color: navy;text-align:justify;font-size: 15px;">
                <div class="col-md-4">
                    <img src="assets/img/conf1.jpg" style="width: 100%">
                </div>
                <div class="col-md-8">
                    <h4>National Conference "JCON 2009" 21 & 22 February 2009</h3>
                    <p>National Conference "JCON 2009" 21 & 22 February 2009 on the topic "Futuristic Shape of Teaching and Training of Jarahat (Surgery) at UG & PG Education of Unani System of Medicine" - Chief Guest - Honorable Smt. Panabaka Lakshmi, Minister of State, Health & Family Welfare, Govt. of India, New Delhi

					</p>

                </div>
            </div>
            <div class="row conf-posts" style="color: navy;text-align:justify;font-size: 15px;">
                <div class="col-md-4">
                    <img src="assets/img/conf1.jpg" style="width: 100%">
                </div>
                <div class="col-md-8">
                    <h4>2017-18 Technology and the Corporation Conferences</h3>
                    <p>The department is well equipped with the latest facilities of Endoscopy, Cystoscopy, Laparoscopy, Research laboratory, Seminar Library and attached with a 95 bedded hospital having outdoor and indoor medical care under different specialties and fully furnished operation theatre.</p>

                </div>
            </div>
            <div class="row conf-posts" style="color: navy;text-align:justify;font-size: 15px;">
                <div class="col-md-4">
                    <img src="assets/img/conf1.jpg" style="width: 100%">
                </div>
                <div class="col-md-8">
                    <h4>2017-18 Technology and the Corporation Conferences</h3>
                    <p>The department is well equipped with the latest facilities of Endoscopy, Cystoscopy, Laparoscopy, Research laboratory, Seminar Library and attached with a 95 bedded hospital having outdoor and indoor medical care under different specialties and fully furnished operation theatre.</p>

                </div>
            </div>
            <button type="button" class="btn btn-secondary">Show More</button>
        </div>
</div>

</div>
<?php
include 'footer.php';
?>