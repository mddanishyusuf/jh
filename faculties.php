
<?php
include 'header.php';
?>
<div class="jh-home-content">
 <div class="row">
   
<div class="col-md-12">
                    <center><h3 class="jh-content-title" style="color:#800000;"><span>List of Faculty Members</span></h3>   
                    </center>    
                        <div class="space"></div>
                                        
                            <div class="row">
                                <div class="col-md-12 compscroll">
                                    <ul>                                                    
                                        <li class="fixed"><img src="https://www.amu.ac.in/empphoto/10068791.jpg" width="120px" height="150px"/></li>
                                        <li>
                                        <p class="compdetail">
												<a href="https://www.amu.ac.in/dshowfacultydata.jsp?did=90&amp;eid=10068791"><strong>Prof. Tafseer Ali</strong>
												</a><br> Professor and Chairman<br/>tafseerali@gmail.com<br/>
												Qualification:M.S. Jarahat (Surgery), M.B.A. Health Care Services, M.Sc in Clinical Research & Regulatory Affaires, P.G. Diploma in Preventive & Promotive Health Care
												<br>started my career as House Officer in the department of Jarahat (Surgery), Faculty of Unani Medicine, Aligarh Muslim University, Aligarh in 1992 and later on joined the same department as lecturer in 1994<br>
												<a href="https://www.amu.ac.in/dshowfacultydata.jsp?did=90&amp;eid=10068791"> Know More </a>
																		

										</p> 

										</li>
                                         
										   
                                        <li class="fixed"><img src="https://www.amu.ac.in/empphoto/10070007.jpg" width="120px" height="150px"/></li>
                                        <li>
																		<p class="compdetail"> 	<a href=https://www.amu.ac.in/dshowfacultydata.jsp?did=90&eid=10058929 ><strong>
																												Prof. Iqbal Aziz</strong> </a>
														</a><br> Professor <br>mail.iqbalaziz@gmail.com<br/>
														Qualification:M.S. (General Surgery)<br>He is attached to the surgery department of the Unani Faculty since 1993<br>
											<a href="https://www.amu.ac.in/dshowfacultydata.jsp?did=90&eid=100100007" > Know More </a></p>

                                        </li>
                                                                                                    
                                        <li class="fixed"><img src="https://www.amu.ac.in/empphoto/10069844.jpg"  width="120px" height="150px"/></li>
                                        <li>
                                        <p class="compdetail"> <a href=https://www.amu.ac.in/dshowfacultydata.jsp?did=90&eid=10069844 ><strong>Dr. Atif Anwar</strong> </a>
						</a><br> Assistant Professor <br>atifanwarali@hotmail.com<br/>
						Qualification:MBBS,DO,DNB (Ophthalmology)<br>Thrust Area  :Cataract, Glaucoma<br>
			<a href=https://www.amu.ac.in/dshowfacultydata.jsp?did=90&eid=10069844 > Know More </a>
										</p>
                                        </li>
										<li class="fixed"><img src="https://www.amu.ac.in/empphoto/10072766.jpg" width="120px" height="150px"/></li>
                                        <li>
                                        <p class="compdetail"><a href=https://www.amu.ac.in/dshowfacultydata.jsp?did=90&eid=10072766 ><strong>Dr. Rahida Hilal</strong> </a>
												</a><br> Assistant Professor <br>rahidahilal@gmail.com<br>
												Qualification: M.S. (Jarahat) <br>Thrust Area   :	General Surgery<br>
									<a href=https://www.amu.ac.in/dshowfacultydata.jsp?did=90&eid=10072766 > Know More </a>  </p>
                                        </li>
										<li class="fixed"><img src="https://www.amu.ac.in/empphoto/10065592.jpg" width="120px" height="150px"/></li>
                                        <li>
                                        <p class="compdetail">  <a href=https://www.amu.ac.in/dshowfacultydata.jsp?did=90&eid=10065592 ><strong>Dr. SIMEEN USMANI</strong> </a>
						</a><br> Assistant Professor <br>drehtisham777@rediffmail.com<br>						
Qualification :	MBBS, DNB<br>Thrust Area   :	Anaesthesiology<br>
			<a href=https://www.amu.ac.in/dshowfacultydata.jsp?did=90&eid=10065592 > Know More </a>
										</p>
                                        </li>
										<li class="fixed"><img src="https://www.amu.ac.in/empphoto/10072364.jpg" width="120px" height="150px"/></li>
                                        <li>
                                        <p class="compdetail">
					 <a href=https://www.amu.ac.in/dshowfacultydata.jsp?did=90&eid=10072364 ><strong>Dr. Yasmeen Aziz </strong> 
						 </a><br> Assistant Professor <br>yasmeenaziz1972@gmail.comp<br>
						 	Qualification :	M.B.B.S., M.S (ENT)<br>Thrust Area   :	Interaction between Unani and Allopathy in treating chronic diseases in Ear, Nose, and Throat<br>
						 <a href=https://www.amu.ac.in/dshowfacultydata.jsp?did=90&eid=10072364 > Know More </a>  </p>
                                        </li>

                                                                                    </ul>                               </div>
                            </div>
                    </div>   
                   
			</div>

</div>
<?php
include 'footer.php';
?>