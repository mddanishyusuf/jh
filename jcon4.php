
<!doctype HTML>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="stylesheet" href="/reg/node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue-deep_purple.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link href="/reg/fonts/fontello/css/fontello.css" rel="stylesheet" />
    <link rel="stylesheet" href="/reg/css/bootstrap-offset-right.css">
    <link rel="stylesheet" href="/reg/css/style.css">
	
	
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title></title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.css"/>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <style>
    body{
    padding: 5px;
        }
    .mlt-carousel{
    background:#800000;
    }
    
        .mdl-textfield__label {
            margin-bottom: 0;
            color: #7f898b;
            font-weight: normal;
        }
        
        .mdl-textfield--floating-label.is-focused .mdl-textfield__label,
        .mdl-textfield--floating-label.is-dirty .mdl-textfield__label {
            text-transform: uppercase
        }
        
        .has-feedback label~.form-control-feedback {
            top: 15px;
        }
        
        .mdl-textfield {
            width: 100%;
        }
        
        .mdl-checkbox__label {
            cursor: text;
            font-size: 13px;
            float: left;
            color: #b0b3b4;
            font-weight: normal;
        }
        
        .mdl-checkbox__box-outline {
            border: 1px solid #b0b3b4;
        }
        
        .mdl-textfield__input {
            border: none;
            border-bottom: 1px solid rgba(0, 0, 0, .12);
            display: block;
            font-size: 16px;
            margin: 0;
            padding: 4px 0;
            width: 100%;
            background: 0 0;
            text-align: left;
            color: inherit;
            font-weight: bold;
        }
        
        .mdl-switch__label {
            float: left;
            font-weight: normal;
            color: #b0b3b4;
            font-size: 14px;
        }
        .compdetail{
                //width:300px;
                height:470px;
                overflow-y:scroll;
                color:#000;
                //text-align:justify;
                padding:6px;
                font-size:0.85em;
            }
.compdetail::-webkit-scrollbar {
    width: 12px;
}
 
.compdetail::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
    border-radius: 10px;
}
 
.compdetail::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
}
.input-group-addon {
width: 40px;
}
.psp {
display:none;
}
    </style>
</head>

<body>
<div class="container">
  <div class="row jh-header">
    <div class="col-md-2;margin:1px;">
      <img src="https://www.amu.ac.in/img/logo.png" style="background-color: #800000;">
    </div>
    <div class="col-md-9" style="font-size: 27px;padding-top: 5.5%;padding-bottom: 2.5%;color: #fff;background-color:#800000;font-weight: bold;margin:1px;">
      <center>JCON-4 International Conference</center>
    </div>
  </div>


    <div class="container">
        <div class="center-block">
            
            <!-- Login -->
            
            <div class="container">

<fieldset>

<!-- Form Name -->
<legend><b>Register!</b></legend>

<!-- Text input-->
<?php 
if(!empty($_SESSION['error'])) {?>
<!-- Success message -->
<div class="alert alert-danger" role="alert" id="failure_message">Failure <i class="glyphicon glyphicon-thumbs-down"></i> <?=$_SESSION['error']?>.</div>
<?php 
}
?>
<!--Text Input-->
<div class="form-group">
  <label class="col-md-4 control-label"><b>Name</b></label>  
  <div class="col-md-4 inputGroupContainer">
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input  name="name" id='name' placeholder=" Enter your full Name" class="form-control"  type="text">
    <span id='name1' style='margin-top:3px;'></span>
    </div>
  </div>
</div>

<!-- radio checks -->
 <div class="form-group">
                        <label class="col-md-4 control-label"><b>Gender:</b></label>
                        <div class="col-md-4">
                            <div class="radio">
                                <label for='gender_m'>
                                    <input type="radio" name="gender" id='gender_m' value="m" /> Male
                                </label>
                                <label for='gender_f'>
                                    <input type="radio" name="gender" id='gender_f' value="f" /> Female
                                </label>
                            </div>
                            <label for="gender" class="error"></label>
                        </div>
                    </div>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label" ><b>Father's Name</b></label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input name="father_name" id='father_name' placeholder="Father's Name" class="form-control"  type="text">
    <span id='father_name1' style='margin-top:3px;'></span>
    </div>
  </div>
</div>

<!-- Text input-->
       <div class="form-group">
        <label class="col-md-4 control-label"><b>E-Mail</b></label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <input name="email" id='email' placeholder="E-Mail Address" class="form-control"  type="text">
                <span id='email1' style='margin-top:3px;'></span>
            </div>
        </div>
        </div>


        <!-- Text input-->
            
        <div class="form-group">
        <label class="col-md-4 control-label"><b>Mobile Number</b></label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                <input name="mob" id='mob' placeholder="Mobile number with country code" class="form-control" type="text">
                <span id='mob1' style='margin-top:3px;'></span>
            </div>
        </div>
        </div>

        <!-- Text input-->
            
        <div class="form-group">
        <label class="col-md-4 control-label"><b>Address</b></label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                <input name="address" id='address' placeholder="Enter your full Address" class="form-control" type="text">
                <span id='address1' style='margin-top:3px;'></span>
            </div>
        </div>
        </div>

        <div class="form-group"> 
        <label class="col-md-4 control-label"><b>Registration Type</b></label>
            <div class="col-md-4 selectContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
            <select name="type" id='type' class="form-control selectpicker" >
            <option value="" >Select</option>
            <option value="student">Students/Intern</option>
            <option value="teacher">Teacher/Researcher</option>
            <option value="spot" >On Spot</option>
            <option value="industry" >Industry</option>
            <option value="international" id="international" >International Delegates</option>
            </select>
            <span id='type1' style='margin-top:3px;'></span>
        </div>
        </div>
        </div>

        <div class="form-group"> 
        <label class="col-md-4 control-label"><b>Accompanying Person</b></label>
            <div class="col-md-4 selectContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
            <select name="accompanying_person" id='accompanying_person' class="form-control selectpicker" >
            <option value="" >Select</option>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2" >2</option>
            <option value="3" >3</option>
            </select>
            <span id='accompanying_person1' style='margin-top:3px;'></span>
        </div>
        </div>
        </div>

        <!-- Text input-->
        <div class="form-group psp">
        <label class="col-md-4 control-label"><b>Passport Number</b></label>  
        <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                <input name="passport_no" id='passport_no' placeholder="Passport Number" class="form-control" type="text">
                <span id='passport_no1' style='margin-top:3px;'></span>
            </div>
        </div>
        </div>

        <!-- Text input-->
        <div class="form-group psp">
        <label class="col-md-4 control-label"><b>Issuing Office</b></label>  
        <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></span>
                <input name="issue_office" id='issue_office' placeholder="Issuing Office" class="form-control" type="text">
                <span id='issue_office1' style='margin-top:3px;'></span>
            </div>
        </div>
        </div>

        <div class="form-group psp"> <!-- Date1 input -->
                <label class="col-md-4 control-label" for="issue_date"><b>Passport Issue Date:</b></label>
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group" id="sandbox-container1">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                <input class="form-control" id="issue_date" name="issue_date" placeholder="MM/DD/YYY" type="text"/>
                <span id='issue_date1' style='margin-top:3px;'></span>
                </div>
            </div>
        </div>

        <div class="form-group psp"> <!-- Date2 input -->
                <label class="col-md-4 control-label" for="valid_upto"><b>Valid Upto Date:</b></label>
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group" id="sandbox-container2">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                <input class="form-control" id="valid_upto" name="valid_upto" placeholder="MM/DD/YYY" type="text"/>
                <span id='valid_upto1' style='margin-top:3px;'></span>
                </div>
            </div>
        </div>

        <div class="form-group psp"> <!-- Passport Scan input -->
                <label class="col-md-4 control-label" for="exampleInputFile"><b>Upload Passport Scan: (Supported: jpg,png,pdf)</b></label>
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group">
                    <input class="form-control" id="exampleInputFile" name="pass_scan" type="file" accept="image/jpeg,image/pjpeg,image/png,application/pdf" />
                    <span id='exampleInputFile1' style='margin-top:3px;'></span>
                </div>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
        <label class="col-md-4 control-label"><b>Abstract Title</b></label>  
        <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
                <input name="abstract" id='abstract' class="form-control" type="text">
                <span id='abstract1' style='margin-top:3px;'></span>
            </div>
        </div>
        </div>


        <div class="form-group"> <!-- Abstract input -->
                <label class="col-md-4 control-label" for="abstract_file"><b>Upload Abstract: (Supported: pdf,doc,docx,txt)</b></label>
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group">
                    <input class="form-control" id="abstract_file" name="abstract_file" type="file" accept="application/pdf,application/msword,text/plain,application/vnd.openxmlformats-officedocument.wordprocessingml.document" >
                    <span id='abstract_file1' style='margin-top:3px;'></span>
                </div>
            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4">
            <button type="submit" class="btn btn-warning" >Register <span class="glyphicon glyphicon-send"></span></button>
        </div>
        </div>

    </fieldset>
</form>
</div>


          
                <!--Login-->
            </div>
            <!--center-block-->
        </div>
        <!--container-->
    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script>
    $('#sandbox-container1 input').datepicker({
        autoclose: true
    });

    $('#sandbox-container1 input').on('show', function(e){
        console.debug('show', e.date, $(this).data('stickyDate'));
        
        if ( e.date ) {
            $(this).data('stickyDate', e.date);
        }
        else {
            $(this).data('stickyDate', null);
        }
    });

    $('#sandbox-container1 input').on('hide', function(e){
        console.debug('hide', e.date, $(this).data('stickyDate'));
        var stickyDate = $(this).data('stickyDate');
        
        if ( !e.date && stickyDate ) {
            console.debug('restore stickyDate', stickyDate);
            $(this).datepicker('setDate', stickyDate);
            $(this).data('stickyDate', null);
        }
    });
    

    //second date
    $('#sandbox-container2 input').datepicker({
        autoclose: true
    });

    $('#sandbox-container2 input').on('show', function(e){
        console.debug('show', e.date, $(this).data('stickyDate'));
        
        if ( e.date ) {
            $(this).data('stickyDate', e.date);
        }
        else {
            $(this).data('stickyDate', null);
        }
    });

    $('#sandbox-container2 input').on('hide', function(e){
        console.debug('hide', e.date, $(this).data('stickyDate'));
        var stickyDate = $(this).data('stickyDate');
        
        if ( !e.date && stickyDate ) {
            console.debug('restore stickyDate', stickyDate);
            $(this).datepicker('setDate', stickyDate);
            $(this).data('stickyDate', null);
        }
    });
    </script>

    <script>
    $('#type').change(function(){
        if(this.value == 'international') {
            $( ".psp" ).addClass('ssp').removeClass( "psp" );
        }
        else {
            $( ".ssp" ).addClass( "psp" ).removeClass( "ssp" );
        }
    });
    </script>


    <script src="/reg/node_modules/jquery/dist/jquery.min.js "></script>
    <script src="/reg/node_modules/bootstrap/dist/js/bootstrap.min.js "></script>
    <script src="/reg/libs/mdl/material.min.js "></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js'></script>
    <script>
        //Google analytics.
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        // ga('create', 'UA-79865537-1', 'auto');
        // ga('send', 'pageview');
    </script>
    <script src='/assets/js/reg_validator.js'></script>
    <script>
        (function(){
            if(document.getElementById("international").selected){
                document.getElementById("fileInput").style.display = 'block'
            }
        })();
    </script>

</body>

</html>