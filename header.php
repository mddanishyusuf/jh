<!doctype html>
<html lang="en">

<head>
    <title>DEPARTMENT OF JARAHAT || AMU ALigarh</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="shortcut icon" href="http://www.amucontrollerexams.com/mono.ico">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue-deep_purple.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link href="/reg/fonts/fontello/css/fontello.css" rel="stylesheet" />
    <link rel="stylesheet" href="/reg/css/bootstrap-offset-right.css">
    <link rel="stylesheet" href="/reg/css/style.css">
	
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.css"/>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
	<style>
	body { 
	background: #FDFDFD; 
}
body, input, textarea { 
	font: 14px/24px Helvetica, Arial, sans-serif; 
	color: #666; 
}
input { 
	width: 60% 
}
form { 
	margin: 30px 0 0 0 
}
input, textarea { 
	background: none repeat scroll 0 0 #FFFFFF; 
	border: 1px solid #C9C9C9; 
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset, -5px -5px 0 0 #F5F5F6, 5px 5px 0 0 #F5F5F6, 5px 0 0 0 #F5F5F6, 0 5px 0 0 #F5F5F6, 5px -5px 0 0 #F5F5F6, -5px 5px 0 0 #F5F5F6; 
	color: #545658; 
	padding: 8px; 
	font-size: 14px; 
	border-radius: 2px 2px 2px 2px; 
}
#submit { 
	background: url("../images/submit_bg.gif") repeat-x scroll 0 0 transparent; 
	border: 1px solid #B7D6DF; 
	border-radius: 2px 2px 2px 2px; 
	box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1); 
	color: #437182; 
	cursor: pointer; 
	font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; 
	font-size: 14px;
	font-weight: bold; 
	height: auto; 
	padding: 6px 10px; 
	text-shadow: 0 1px 0 #FFFFFF; 
	width: auto; 
}
#submit:hover { 
	background: url("../images/submit_hover_bg.gif") repeat-x scroll 0 0 transparent; 
	border: 1px solid #9FBAC0; 
	cursor: pointer; 
}
form label { 
	display: block; 
	margin-bottom: 5px; 
	font-weight: bold; 
	font-size: 12px; 
}
   body{
    padding: 5px;
        }
    .mlt-carousel{
    background:#800000;
    }
    
        .mdl-textfield__label {
            margin-bottom: 0;
            color: #7f898b;
            font-weight: normal;
        }
        
        .mdl-textfield--floating-label.is-focused .mdl-textfield__label,
        .mdl-textfield--floating-label.is-dirty .mdl-textfield__label {
            text-transform: uppercase
        }
        
        .has-feedback label~.form-control-feedback {
            top: 15px;
        }
        
        .mdl-textfield {
            width: 100%;
        }
        
        .mdl-checkbox__label {
            cursor: text;
            font-size: 13px;
            float: left;
            color: #b0b3b4;
            font-weight: normal;
        }
        
        .mdl-checkbox__box-outline {
            border: 1px solid #b0b3b4;
        }
        
        .mdl-textfield__input {
            border: none;
            border-bottom: 1px solid rgba(0, 0, 0, .12);
            display: block;
            font-size: 16px;
            margin: 0;
            padding: 4px 0;
            width: 100%;
            background: 0 0;
            text-align: left;
            color: inherit;
            font-weight: bold;
        }
        
        .mdl-switch__label {
            float: left;
            font-weight: normal;
            color: #b0b3b4;
            font-size: 14px;
        }
        .compdetail{
                //width:300px;
                height:470px;
                overflow-y:scroll;
                color:#000;
                //text-align:justify;
                padding:6px;
                font-size:0.85em;
            }
.compdetail::-webkit-scrollbar {
    width: 12px;
}
 
.compdetail::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
    border-radius: 10px;
}
 
.compdetail::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
}
.input-group-addon {
width: 40px;
}
.psp {
display:none;
}
</style>

</head>

<body style="background-color:#6666662b;">
    <div class="container">
        <div class="row jh-header">
            <div class="col-md-2">
             <img src="https://www.amu.ac.in/img/logo.png" style="background-color: #800000;">
            </div>
			<div class="col-md-10">
			<div style="height: 28%;background-color: #fff;">
				<marquee style="    padding-top: 2%;    color: red; font-weight:bold;" onmouseover="this.stop();" onmouseout="this.start();">
				International conference will be organized on 26t<sup>th</sup> Feb to 28<sup>th</sup> Feb 2018 <a href="register.php">Register Here</a> </marquee>  
			</div>
				<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:1px;background-color:#800000;">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li >
							<a class="nv" href="/" >Home </a>
						</li>
						<li class="nav-item">
							<a class="nv" href="/dept" >Department</a>
						</li>
						 <li class="nav-item">
							<a class="nv" href="/news">News</a>
						</li>
						
						<li class="nav-item dropdown">
							<a class="nv dropdown-toggle" style="a:hover{background-color:#800000;}" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Faculty <span class="sr-only">(current)</span></a>
							<div class="nv1 dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="nv1 dropdown-item" href="/faculties">List of Faculties</a>
								<a class="nv1 dropdown-item" href="/research_works">Faculties Research Work</a>
								
							   <!-- <div class="dropdown-divider"></div> -->
							   
							</div>
						</li>
						
						<li class="nav-item dropdown">
							<a class="nv dropdown-toggle" style="a:hover{background-color:#800000;}" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Journal <span class="sr-only">(current)</span></a>
							<div class="nv1 dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="nv1 dropdown-item" href="/journal">International Journals</a>
								<div class="nv1 dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="nv1 dropdown-item" href="/journal">International Journals</a>
								
								<a class="nv1 dropdown-item" href="/publications">Faculties Publications</a>
								
							   <!-- <div class="dropdown-divider"></div> -->
							   
							</div>
								<a class="nv1 dropdown-item" href="/publications">Faculties Publications</a>
								
							   <!-- <div class="dropdown-divider"></div> -->
							   
							</div>
						</li>
						
						<li class="nav-item dropdown">
							<a class="nv dropdown-toggle" style="a:hover{background-color:#800000;}" href="conferences.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Events <span class="sr-only">(current)</span></a>
							<div class="nv1 dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="nv1 dropdown-item" href="/conferences">Conference</a>
								<a class="nv1 dropdown-item" href="/symposium">Symposium</a>
								<a class="nv1 dropdown-item" href="/seminar">Seminar</a>							
								<a class="nv1 dropdown-item" href="/exhibition">Exhibition</a>													
								<a class="nv1 dropdown-item" href="/workshop">Workshops</a>
								<a class="nv1 dropdown-item" href="/guest-lectures">Guest Lectures</a>
								
							   <!-- <div class="dropdown-divider"></div> -->
							   
							</div>
						</li>
						<li class="nav-item">
							<a class="nv" href="/gallery">Gallery</a>
						</li>
						<li class="nav-item">
							<a class="nv" href="/contact">Contact Us</a>
						</li>
						
						<li class="nav-item">
							<a class="nv" href="/conference"><span class="blink_me">JCON 4 Conference *</span></a>
						</li>
						
						<!--li class="nav-item">
							<a class="nav-link disabled" href="#">Disabled</a>
						</li-->
					</ul>
					
				</div>
			</nav>
			
		
			<div style="font-size: 27px;padding-top: 5.5%;padding-bottom: 2.5%;color: #fff;background-color:#800000;font-weight: bold;margin:1px;">
			    <center>DEPARTMENT OF JARAHAT</center>
			</div>
		</div>
        </div>
		</div>