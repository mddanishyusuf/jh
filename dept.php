<?php
include 'header.php';
?>
        
        <div class="jh-home-content" style="top:6%">
            <div class="row">
               
                <div class="col-md-12">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" >
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
							 <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="assets/img/jh1.jpg" alt="First slide" height="420px">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3>Aligarh Muslim University</h3>
                                    <p>Where Tradition and Culture says itself..</p>
                                </div>
                            </div>						
                            <div class="carousel-item">
                                <img class="d-block w-100" src="assets/img/jh1.jpg" alt="Fourth slide" height="420px">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3>Department of Jarahat</h3>
                                    <p>You are welcome here</p>
                                </div>
                            </div>
							<div class="carousel-item">
                                <img class="d-block w-100" src="assets/img/event.png" alt="Second slide" height="420px">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3>Department of Jarahat</h3>
                                    <p>Events & Conference</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="assets/img/jarahat.jpg" alt="Third slide" height="420px">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3>A K Tibbiya College</h3>
                                    <p>Faculty of Unani Medicine</p>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
				</div>	
		
		<div class="col-md-12" id="dept" style="text-align:justify">
		<center><h3 class="jh-content-title" style="color:#800000;"><span>About Department</span></h3></center>
		<div style="color: navy;text-align:justify;font-size: 15px;">
				<p>Department of Jarahat, established in 1986, is one of the departments of the faculty of Unani Medicine, Aligarh Muslim University, Aligarh, India.
				The department is providing teaching and clinical training of Ilmul Jarahat (Surgery) to undergraduate and research students.</p>
				<p>The department has a unique and fine integration of traditional Unani and latest scientific knowledge of the field with the vision of ultimate
				development of Unani Medicine and to explore the best possible treatment option of the disease. </p>
				<p> department is well equipped with the latest facilities of Endoscopy, Cystoscopy, Laparoscopy, Research laboratory, Seminar Library and 
				attached with a 95 bedded hospital having outdoor and indoor medical care under different specialties and fully furnished operation theatre.</p>
				<p>The department has teaching and clinical faculties both from Unani & Allopathic. Apart from performing endoscopy, cystoscopy, colonoscopy, open & minimal access 
				surgery the department has been conducting clinical trials on various Unani Medicines as a part of validation and application in contemporary era. 
				We are actively engaged in exploring the Surgical & Medicinal treatise in Unani Medicine for research and development in this field.</p>
				<p>Since its establishment, the department has Produced 28 Researchers/Teachers/Surgeons who are well settled and doing well in the field of Ilmul Jarahat (surgery).
				The department has successfully organized 03 National Conferences, completed 02 Research projects sponsored by the ministry of AYUSH, Govt. of India.
				The department is also doing well in the field of patient care.</p>
				<p> Mentioning the data of last one year (2016), 28544 patients attended OPD, 775 were admitted in IPD, 617 surgeries, 10 cystoscopies, 205 endoscopies and
				4521 hematological and biochemical investigations were done in the Department.</p>
		 </div>
		 </div><!--.module_cont -->
		

					
				
                   
</div>
<?php
include 'footer.php';
?>