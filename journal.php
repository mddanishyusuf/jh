<?php
include 'header.php';
?>

        
        <div class="jh-home-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="jh-content-title" style="color:#800000;"><h2>Journals</h2></div>
                    <ul class="jh-conf-list">
						<li><a href="#upload" onclick="check();return false;">Upload Journals</a></li>
						 <li><a href="#list" onclick="check3();return false;">List</a></li>
                       
                        <li><a href="#iqb" onclick="check1();return false;">Prof. Iqbal Aziz's Journals</a></li>
                        <li><a href="#sim" onclick="check2();return false;">Dr. Simeen Usmani's Journals</a></li>
                       
                    </ul>
					<script>
						function check(){
							document.getElementById("upload").style.display="block";
							document.getElementById("journal").style.display="none";
							
						}
						function check1(){
							document.getElementById("upload").style.display="none";
							document.getElementById("journal").style.display="block";
							document.getElementById("sim").style.display="none";
							document.getElementById("iqb").style.display="block";							
							document.getElementById("list").style.display="none";
							
						}
						function check2(){
							document.getElementById("upload").style.display="none";
							document.getElementById("journal").style.display="block";
							document.getElementById("sim").style.display="block";
							document.getElementById("iqb").style.display="none";							
							document.getElementById("list").style.display="none";
						}
						function check3(){
							document.getElementById("upload").style.display="none";
							document.getElementById("journal").style.display="none";
							document.getElementById("sim").style.display="none";
							document.getElementById("iqb").style.display="none";
							document.getElementById("list").style.display="block";
							
							
						}
					</script>
                </div>
                <div class="col-md-9">
						<div class="col-md-12" id="upload" style="display:none;">
						 <center>  <div class="jh-content-title" style="color:#800000;"><h2>Upload</h2></div></center>
					 
							 <form id="send" method="POST" action="reg1.php">
								 <p>

								<input name="name" placeholder="Enter your Full Name" required="" type="text" required>
								 </p>

								
								<p>
								   
								<input placeholder="Email address *" name="email" required="" type="email"> 
								</p>      
								<p>
								<input name="file" required="" type="file"> 
								</p>  
								  <input value="&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;" type="submit"></center>
							</form>
						</div>
						<div class="col-md-12" id="list" style="display:none;">

							 <center>  <div class="jh-content-title" style="color:#800000;"><h2>List of Journals</h2></div>
							 	<table width="100%" border="1px">
							 		<tr>
							 			<th style="color: #0056b3"> S.No</th>
							 			<th style="color: #0056b3">Title</th>
							 			<th style="color: #0056b3">Name</th>

							 		</tr>
							 		<tr>
							 			<td>1</td>
							 			<td>I know</td>
							 			<td>change it</td>
							 			
							 		</tr>


							 	</table>
					 
							
					</div>
					<br/>
					<div class="col-md-12" style="display:none;" id="journal">
							<h3 class="jh-content-title"><span>All Journals</span></h3>
							<div class="row conf-posts" id="iqb" style="color: navy;text-align:justify;font-size: 15px;">
							
								<div class="col-md-8">
									<h4>Prof. Iqbal Aziz's Journals</h3>
							<p><ol><li>
												“Role of Cadmium and Lead in Malignant Breast Tumor Development”, International Journal of Recent Scientific Research. 2016(Jan); 7(1): 8363-8368.
								</li>
								<li>
								“Effect of A Unani Drug Compound in Helicobacter Pylori Positive Antral Gastritis”, International Journal for Research in Emerging Science and Technology, Vol. 2, Issue-5, May 2015.
								</li><li>
								“Clinical and Economic Impact of appropriate empirical antibiotic therapy in complicated intrabdominal infections: A retrospective study”, Current Medicine Research & Practice ,2015, GRIPMER.
								</li><li.
								“Role of Unani Drugs in Normalizing Altered Liver Functions in Post Cholecystectomy and Choledocholithotomy Patients”, Hippocratic Journal of Unani Medicine, Vol. 9 (2): 31-44, 2014.
								 </li></li>“Large renal Caculus: A challenge for laparoscopic Surgeons”, Unani Medicus –  An International Journal, Faculty of Unani Medicine, AMU, Aligarh, Vol. 2, No., July – December   2013, pp. 17-21.
								</li><li> “A Clinical Study of Saw Palmetto Berry for its Effects on Maximum (Peak) Flow Rate of Uroflowmetry in the Patients of Benign Prostatic Hypertrophy (BPH)”, Hamdard Medicus, July-September 2013, Vol. 56, No. 3, pp. 5-10.
								</li><li>“Sharbat Buzoori Motadil: An Iso-osmotic Alkalizing Diuretic – An Analytical Study”, Hamdard Mediculs, 2010, Vol. 53, No. 3, pp. 71-74.
								</li><li>“Randomized Comparative Study of Electro Surgical and Conventional Scalpel Incisions in General Surgery”, Hamdard Mediculs, 2010, Vol. 20, No. 25, pp. 15-22.
								<li>“Efficacy of Habb-e-Shifa as Pre-anesthetic Medication”, Hamdard Mediculs, 2009, 87-96.</li>
								</ol>
							</p>
						</div>
					</div>
					
					<div class="row conf-posts" id="sim" style="color: navy;text-align:justify;font-size: 15px;">
						
						<div class="col-md-8">
							<h4>Dr. Simeen Usmani's Journals</h3>
							<p>
		1. Pre-emptive Analgesia 
		Simeen Usmani, R.Bhandari, S.Yajnik Journal of Clinical Practice, Vivekananda Polyclinic and Institute of Medical Sciences, Lucknow, 2008. <br>
		2. Study of Obstetric Patients Admitted to Intensive Care Unit at Tertiary Care Centre in Western Uttar Pradesh: One Year Review 
		Simeen Usmani, Roshan Parween and Shadab Alam 
		International Journal of Contemporary Medical Research Volume 3, Issue 2, February 2016.
							</p>

						</div>
					</div>
           </div>
    </div>
</div>

</div>
<?php
include 'footer.php';
?>