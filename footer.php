<div class="row jh-footer">
	<div class="col-md-3" style="color:#1fe0e8;"><center>
		<img src="https://www.amu.ac.in/empphoto/10068791.jpg" width="120px" height="135px"><br>
	 Prof. Tafseer Ali<br>Professor and Chairman
	 </center>
	</div>
	<div class="col-md-6" style="color:#c9aab7; height:100px;">
	<marquee direction="up"  scrollamount="2" height="180px" onmouseover="this.stop();" onmouseout="this.start();">
	National Conference "JCON 2011" 21 & 22 January 2012 on the topic - Live workshop on Laparoscopic Surgery "Adoption of Advanced Surgical Technology in Indigenous System of Medicine - A Need of Today" - Chief Guest - Dr. Anil Arora, The Director, Sir Ganga Ram Hospital, New Delhi
	<br/><hr color="#fff">
		 National Conference "JCON 2009" 21 & 22 February 2009 on the topic "Futuristic Shape of Teaching and Training of Jarahat (Surgery) at UG & PG Education of Unani System of Medicine" - Chief Guest - Honorable Smt. Panabaka Lakshmi, Minister of State, Health & Family Welfare, Govt. of India, New Delhi

	</marquee>
	</div>
	<div class="col-md-3" style="color:#fff">
	<h4>ALUMNI OF THE DEPARTMENT</h4>
		<marquee direction="up"  scrollamount="2" height="180px" onmouseover="this.stop();" onmouseout="this.start();">
													   
			<p align="justify" > 1.	Minhaj Ahmad, Jamia Hamdard, New Delhi
			</p>



			<p align="justify" > 2.	Darakhshan Khanam, A & U Tibbiya College,Karol Bagh, New Delhi
			</p>



			<p align="justify" > 3.	Mohd. Azam, NIUM, Bangalore
			</p>



			<p align="justify" > 4.	M. Zulkifle, NIUM, Bangalore
			</p>



			<p align="justify" > 5.	Saiyed Shah Alam, NIUM, Bangalore
			</p>



			<p align="justify" > 6.	Manzoor Ahmad, ZVM Unani Medical College, Pune
			</p>



			<p align="justify" > 7.	Mohd. Shakeel Ansari, NIUM, Bangalore
			</p>



			<p align="justify" > 8.	Mohd. Amin Mir, Red Flag Hospital, Faridabad
			</p>



			<p align="justify" > 9.    Fareha Hasan, Kailash Hospital, Delhi
			</p>



			<p align="justify" > 10.	Mohd. Kashif, Medical Officer, State Govt., U.P.
			</p>



			<p align="justify" > 11.	Muhammad Nadeem Khan, A.K. Tibbiya College,AMU, Aligarh
			</p>



			<p align="justify" > 12.	Arshad Hussain Shah, Medical Officer, State Govt., Jammu & Kashmir
			</p>



			<p align="justify" > 13.	Md. Sabir Alam, Medical Officer, NRHM, Araria, Bihar
			</p>



			<p align="justify" > 14.	Firdaus A. Najar, NIUM, Bangalore
			</p>



			<p align="justify" > 15.	Nishat Ashraf, Jamia Hamdard, New Delhi
			</p>



			<p align="justify" > 16.	Mohd. Abu Bakr Qadri, A & U Tibbiya College, Karol Bagh, New Delhi
			</p>




			<p align="justify" > 

			17. Mehjabeen Fatimah, NIUM, Banglore</p>


		</marquee>
	</div>
	
<hr width="100%" color="white">
    <div class="col-md-9">
        <ul class="jh-footer-nav">
            <li >
							<a class="nv" href="/">Home </a>
						</li>
						<li class="nav-item">
							<a class="nv" href="/dept">Department</a>
						</li>
						 <li class="nav-item">
							<a class="nv" href="/news">News</a>
						</li>
						 <li class="nav-item">
							<a class="nv" href="/faculties">Faculties</a>
						</li>
						 <li class="nav-item">
							<a class="nv" href="/journal">Journals</a>
						</li>
						<li class="nav-item">
							<a class="nv" href="/conferences">Conference</a>
						</li>
						<li class="nav-item">
							<a class="nv" href="/gallery">Gallery</a>
						</li>
						<li class="nav-item">
							<a class="nv" href="/contact">Contact Us</a>
						</li>
						
						
						
        </ul>
    </div>
    <div class="col-md-3">        
        <ul class="jh-social">
            <li><a href=""><img src="assets/img/social/fb.png"></a></li>
            <li><a href=""><img src="assets/img/social/tw.jpg"></a></li>
        </ul>
    </div>
	<div class="col-md-6" style="color:#fff;"><center>&copy; Department of Jarahat 2017</center></div>
	<div class="col-md-6" style="color:#fff;"><center> Design by Code4U</center></div>
</div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>