<?php

?>


<!DOCTYPE html>
<html class="no-js">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Admin-Panel | DEPARTMENT OF JARAHAT </title>

<link rel="shortcut icon" href="http://www.amucontrollerexams.com/mono.ico">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="asset/js/html5.js"></script>
<![endif]-->

<!-- core js files -->
<script src="asset/js/jquery-1.11.0.min.js"></script>
<script src="asset/js/bootstrap.min.js"></script>
<script src="asset/js/modernizr.custom.js"></script>
<script src="asset/js/core.js"></script>
<!-- core js files -->

<link rel="stylesheet" href="asset/css/bootstrap.min.css">
<link rel="stylesheet" href="asset/css/font-awesome.min.css">
<link rel="stylesheet" href="asset/css/style.css">


<link rel="stylesheet" href="asset/css/plugins/chosen/chosen.css">
<script src="asset/js/plugins/chosen.jquery.min.js"></script>

<link rel="stylesheet" href="asset/datatable/media/css/demo_table.css">
<link rel="stylesheet" href="asset/css/dtable.css">
<script src="asset/datatable/media/js/jquery.dataTables.min.js"></script>
<script src="asset/datatable/media/js/sorting.js"></script>

<script src="asset/js/plugins/jquery.tipsy.js"></script>
<link rel="stylesheet" href="asset/css/plugins/files/tipsy.css">

<link rel="stylesheet" href="asset/css/plugins/datepicker/datepicker.css">
<script src="asset/js/plugins/bootstrap-datepicker.js"></script>
<script src="asset/js/plugins/jquery.maskedinput.min.js"></script>

<script src="asset/js/plugins/bootstrap3-typeahead.min.js"></script>

<script src="asset/js/plugins/bootbox.min.js"></script>
<script src="asset/js/plugins/jquery.dlmenu.js"></script>

<link rel="stylesheet" href="asset/css/plugins/files/bootstrap-checkbox.css">
<script src="asset/js/plugins/bootstrap-checkbox.js"></script>

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

<script type="text/javascript">
    var ac_siteURL='';  //PHP Application üzerinde autocomplete verisi işlenmesi için temel adres tanımlaması için
</script>

</head>
<body>
<div id="application" style="overflow: auto;">
    <div id="topLine">
        <div class="applogo">
          
        </div>

        <div class="topcontent hidden-xs">
           <a href="index.php"> <span style="font-size:21px; font-weight:700"> DEPARTMENT OF JARAHAT</a></span>


        </div>
        <div class="topmenu">
            <ul>
                <li>
                    <a href="#" class="ta">
                    <img src="asset/img/ac.jpg" width="50px" class="img-circle" />
                    <span>Admin</span>
                    <i class="fa fa-sort-desc"></i>
                    </a>
                    <ul>
                        <li><a href="../index.php" target="_blank">dp</a></li>
                        <li><a href="logout.php">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

        <!--<div class="appmenu dl-menuwrapper">-->
            <!--<ul id="menu" class="dl-menu">-->
                <!--<li><a href="#"><i class="fa fa-tachometer"></i>Ana sayfa</a></li>-->
                <!--<li><a href="#" class="active"><i class="fa fa-user"></i>Kullanıcılar</a></li>-->
                <!--<li><a href="#"><i class="fa fa-list-alt"></i>Formlar</a>-->
                    <!--<ul class="dl-submenu">-->
                        <!--<li><a href="#" class="active">Sub 1</a></li>-->
                        <!--<li><a href="#">Sub 2</a></li>-->
                        <!--<li><a href="#">Sub 3</a></li>-->
                    <!--</ul>-->
                <!--</li>-->
            <!--</ul>-->
        <!--</div>-->
        <div class="appcontent" >
            <div class="appbreadcrumb">
                <ol class="breadcrumb">

                    <li><a href="index.php">Home</a></li>
                    <li class="active"><a href="index.php">News</a></li>
                    <li class="active"> <a href="index.php">Department</a></li>
                    <li><a href="jcon4.php">Conference</a></li>
                    <li><a href="second.php">Gallery</a></li>
                    <li><a href="second.php">Faculties</a></li>
                      <li><a href="third.php">Events</a></li>
                    <li><a href="index.php#cont">Contact Us</a></li>
                    
                </ol>
            </div>
            <div class="message">
            <!----    <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Warning!</strong> Better check yourself, you're not looking too good.
                </div>---->
            </div>
            <div class="content">
                <div class="row">



                    <!----              News                -->


                    <div class="col-md-4">
                        <div class="widget">
                            <div class="whead">
                                <h6><i class="fa fa-cloud"></i>    <span style="color:#e74c3c; font-weight:700 "> Add News</span></h6>
                                <div class="btn-group">
                                    <a href="edit.php" class="btn btn-primary" data-message=""><i class="fa fa-share"></i> Edit News</a>
                                </div>
                            </div>
                            <div class="wbody">
                            <form action="news.php" method="GET">
                                <table class="table">
                                    <tr>
                                    <span style="color:#e74c3c; font-weight:700">News Update</span>
                                    </tr>
                                    <tr>
                                       
                                         <td><b><span style="color:green; font-weight:700; font-size:29; ">  Date:</span></b></td>

                                        <td><input type="date" class="form-control" name="sid" value=""  required/></td>
                                        
                                    </tr>
                                    <tr>
                                         <td><b><span style="color:green; font-weight:700; font-size:29; ">  News:</span></b></td>

                                        <td><textarea cols="42" rows="4" ></textarea></td>
                                    </tr>
                                     <tr>
                                        <td></td>
                                        <td>  <input type="submit" class="btn btn-success" value="submit"></td>

                                    </tr>


                                </table>
                            </form>
                            </div>
                        </div>
                    </div>

                    <!----              Department                -->


                      <div class="col-md-6">
                        <div class="widget">
                            <div class="whead">
                                  <div class="btn-group" style="float: left;">
                                    <a href="edit.php" class="btn btn-primary" data-message=""><i class="fa fa-share"></i>Edit Department Page</a>
                                </div>
                            </div>
                            <div class="wbody">
                            <form action="news.php" method="GET">
                                <table class="table">
                                    <tr>
                                    <span style="color:#e74c3c; font-weight:700">DEPARTMENT OF JARAHAT</span>
                                    </tr>
                                    <tr>
                                       
                                         <td><b><span style="color:green; font-weight:700; font-size:29; ">1<sup>st</sup> Paragraph</span></b></td>

                                        <td><textarea cols="36" rows="4" ></textarea></td>
                                         <td>
                                            <a href="#" class="btn btn-primary" data-message="">Click to add More Paragraph</a>
                                        </td>
                                        
                                    </tr>
                                     
                                   
                                     <tr>
                                        
                                        <td>  <input type="submit" class="btn btn-success" value="Update"></td>

                                    </tr>


                                </table>
                            </form>
                            </div>
                        </div>
                    </div>



                    <!----              Contact Us Page                -->



                    
                    <div class="col-md-4" id="cont">
                        <div class="widget">

                            <div class="whead">
                               <div class="btn-group" style="float: left">
                                    <a href="second.php" class="btn btn-primary" data-message=""><i class="fa fa-share"></i> Edit Contact Us Page</a>
                                </div>
                               
                            </div>
                            <div class="wbody">
                            <form action="news.php" method="GET">
                                <table class="table">
                                    <tr>
                                    <span style="color:#e74c3c; font-weight:700"></span>
                                    </tr>
                                    <tr>
                                       
                                         <td><b><span style="color:green; font-weight:700; font-size:29; ">  Details:</span></b></td>
                                         <td><textarea cols="42" rows="4" ></textarea></td>
                                        
                                        
                                    </tr> 
                                    <tr>
                                       
                                         <td><b><span style="color:green; font-weight:700; font-size:29; ">  Email:</span></b></td>
                                         <td><input type="text" name="email"></td>
                                        
                                        
                                    </tr>

                                    <tr>
                                       
                                         <td><b><span style="color:green; font-weight:700; font-size:29; ">  Contact:</span></b></td>

                                        <td><input type="text" name="confimg"></td>
                                        
                                    </tr> 

                                     <tr>
                                        <td></td>
                                        <td>  <input type="submit" class="btn btn-success" value="submit"></td>

                                    </tr>


                                </table>
                            </form>
                            </div>
                        </div>
                    </div>




                    <!----              Add Alumni                -->


                      <div class="col-md-6">
                        <div class="widget">
                            <div class="whead">
                                  <div class="btn-group" style="float: left;">
                                    <a href="edit.php" class="btn btn-primary" data-message=""><i class="fa fa-share"></i>Add Alumni in the List</a>
                                </div>
                            </div>
                            <div class="wbody">
                            <form action="alum.php" method="GET">
                                <table class="table">
                                    <tr>
                                    <span style="color:#e74c3c; font-weight:700">DEPARTMENT OF JARAHAT</span>
                                    </tr>
                                    <tr>
                                       
                                         <td><b><span style="color:green; font-weight:700; font-size:29; ">Alumni Name</span></b></td>

                                        <td><input type="text" name="name"></td>
                                        
                                        
                                    </tr>
                                    <tr>
                                       
                                         <td><b><span style="color:green; font-weight:700; font-size:29; ">  Mobile:</span></b></td>
                                         <td><input type="text" name="mob"></td>
                                        
                                        
                                    </tr> 
                                    <tr>
                                       
                                         <td><b><span style="color:green; font-weight:700; font-size:29; ">  Email:</span></b></td>
                                         <td><input type="text" name="email"></td>
                                        
                                        
                                    </tr>

                                    <tr>
                                       
                                         <td><b><span style="color:green; font-weight:700; font-size:29; ">  Facebook Id:</span></b></td>

                                        <td><input type="text" name="confimg"></td>
                                        
                                    </tr> 
                                     
                                      <tr>
                                       
                                         <td><b><span style="color:green; font-weight:700; font-size:29; ">  Work Location:</span></b></td>

                                        <td><textarea cols="42" rows="4"></textarea></td>
                                        
                                    </tr> 
                                     
                                   
                                     <tr>
                                        
                                        <td>  <input type="submit" class="btn btn-success" value="Update"></td>

                                    </tr>


                                </table>
                            </form>
                            </div>
                        </div>
                    </div>





                        </div>
                    </div>


                          <br><br>
                    <p align="center"><span style="color:#e74c3c; font-weight:700">If you have any Technical Problem</span> <span style="color:#357ebd; font-weight:700">Contact us:<br> </span></p>

                <div class="clearfix"></div>
                </div>
            <div class="ara"></div>
            </div>
        </div>
<script>
function up(x) {
var z = document.getElementById(x).value;
document.getElementById(x).value = z.toUpperCase();
}
</script>

</body>
</html>
<?php

?>
