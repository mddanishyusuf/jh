<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin-Panel | DEPARTMENT OF JARAHAT </title>

	<link rel="shortcut icon" href="http://www.amucontrollerexams.com/mono.ico">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="asset/js/html5.js"></script>
    <![endif]-->

    <!-- core js files -->
    <script src="asset/js/jquery-1.11.0.min.js"></script>
    <script src="asset/js/bootstrap.min.js"></script>
    <script src="asset/js/modernizr.custom.js"></script>
    <script src="asset/js/core.js"></script>
    <!-- core js files -->

    <link rel="stylesheet" href="asset/css/bootstrap.min.css">
    <link rel="stylesheet" href="asset/css/font-awesome.min.css">
    <link rel="stylesheet" href="asset/css/style.css">
    <link rel="stylesheet" href="asset/css/login.css">


    <link rel="stylesheet" href="asset/css/plugins/chosen/chosen.css">
    <script src="asset/js/plugins/chosen.jquery.min.js"></script>


    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

</head>
<body>

<div class="container">
    <div class="row vertical-center-row">
        <form action="log.php" method="post" >
            <div id="loginarea">
                <h1 align="center">Admin Panel</h1>
				 <h6 align="center">-----Restricted Authorization------</h6>
                <hr/>
                <div class="row">
                    <div class="col-md-4">
                        <div class="image">
                           <a href="../index.php"><img src="https://www.amu.ac.in/img/logo.png" class="img-responsive img-rounded" style="   background: #800000;">
								</a>
							</div>
                    </div>
                    <div class="col-md-8">
					<div class="form-group">
					<span style="color:#e74c3c; font-weight:700"><?php if(isset($_GET['msg'])) echo $_GET['msg']; ?></span>
					
					</div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Username" required />
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="pwd" placeholder="password" required />
                        </div>
                   
					<div class="form-group"><p align="center"><span style="color:green">** Contact Admin for Username(if you don't have) **
					mail-us : <b>jarahat38@rediffmail.com<b></span></p></div>
					</div>
			   </div>
                <hr/>
                <div class="row">
                    <div class="col-md-10"></div>
                    <div class="col-md-2">
                        <input type="submit" class="btn btn-success" value="Login">
                    </div>
                </div>
            </div>
        </form>
		

                              <br><br>
						
						<p align="center"><span style="color:#e74c3c; font-weight:700">If you have any Technical Problem</span> 
                            <br><span style="color:#357ebd; font-weight:700">Contact us:+91-9454131786 </span></p>
    </div>
</div>

</body>
</html>